from src.word_analytics import filter_words_containing_letter_a

def test_filtering_for_empty_list():
    filtered_list = filter_words_containing_letter_a([])
    assert filtered_list == []

def test_filtering_for_list_of_integers():
    filtered_list = filter_words_containing_letter_a([1, 2, 3])
    assert filtered_list == []

def test_filtering_for_list_of_special_characters():
    filtered_list = filter_words_containing_letter_a(['@', '#'])
    assert filtered_list == []

def test_filtering_for_list_not_containing_any_word_with_letter_a():
    filtered_list = filter_words_containing_letter_a(['pies', 'smok', 'kot'])
    assert filtered_list == []

def test_filtering_for_mixed_list_of_single_values():
    filtered_list = filter_words_containing_letter_a(['a', 'b', 'a', 'b'])
    assert filtered_list == ['a', 'a']

def test_filtering_for_list_of_mixed_single_values():
    filtered_list = filter_words_containing_letter_a([1, 'a', 2, 'b'])
    assert filtered_list == ['a']

def test_filtering_for_list_containing_only_words_with_letter_a():
    filtered_list = filter_words_containing_letter_a(['mata', 'krata', 'trata'])
    assert filtered_list == ['mata', 'krata', 'trata']


[]
[1, 2, 3]
['@', '#']
['pies', 'smok', 'kot']
['a', 'b', 'a', 'b']
[1, 'a', 2, 'b']
['mata', 'krata', 'trata']
