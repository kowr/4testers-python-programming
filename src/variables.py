first_name = "Rafał"
last_name = "Kowalik"
age = 34

print(first_name)
print(last_name)
print(age)

#a set of variables describing my dog
# name = "Bandzior"
# age_in_years = 3
# paws = 4
# ears = 2
# is_female = False
#
# print("Mój pies to", name,", he is", age_in_years, "years old. He has", paws, "paws and", ears, "ears.")

# a set of variables describing my best friend
friend_name = "Kleofas"
friend_age_in_years = 44
friend_number_of_animals = 2
friend_has_driving_license = True
friendship_duration_in_years = 4.75

print(friend_name, friend_age_in_years, friend_number_of_animals,
      friend_has_driving_license, friendship_duration_in_years)

print("My friend's name:", friend_name, sep='\t')

