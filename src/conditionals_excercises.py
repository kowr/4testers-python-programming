# ĆWICZENIE 1
def display_speed_information(speed):
    #    if speed <= 50:
    #        print('Thank you! Your speed is OK! :)')
    #    else:
    #        print('SLOW DOWN! :(')

    if speed > 50:
        print('SLOW DOWN! :(')
    else:
        print('Thank you! Your speed is OK! :)')


# ĆWICZENIE 2
def check_temperature_and_pressure(temperature_in_celcius, pressure_in_hectopascals):
    if temperature_in_celcius == 0 and pressure_in_hectopascals == 1013:
        print('True')
    else:
        print('False')

#    age = 18
#
# if ((age>= 8) and (age<= 12)):

# ĆWICZENIE 3
def calculate_fine_amount(speed):
    return 500 + (speed - 50) * 10

def print_the_value_of_speeding_fine_in_built_up_area(speed):
    print('Your speed was:', speed)
    if speed > 100:
        print('mandat milijon złoty i zabieramy prawko Panie Areczku :(')
    elif speed > 50:
        print(f'mały mandacik: {calculate_fine_amount(speed)} złoty polskich :|')
    else:
        print('Szerokiej drogi Panie Kierowco! :)')

# ĆWICZENIE 4 moja propozycja
def return_grade_info(grade):
    if grade >= 2 and grade < 3:
        return 'niedostateczny'
    elif grade >=3 and grade <4:
        return 'dostateczny'
    elif grade >=4 and grade < 4.5:
        return 'dobry'
    elif grade >= 4.5 and grade <=5:
        return 'bardzo dobry'
    else:
        return 'N/A'

# ĆWICZENIE 4 lepsze rozwiązanie - bez zakresów
def return_grade_info_lepsze_rozwiazanie(grade_lepsze_rozwiazanie):
    if not isinstance(grade_lepsze_rozwiazanie, float) or isinstance(grade_lepsze_rozwiazanie, int):
        return 'N/A'
    elif grade_lepsze_rozwiazanie < 2 or grade_lepsze_rozwiazanie > 5:
        return 'N/A'
    elif grade_lepsze_rozwiazanie >= 4.5:
        return 'bardzo dobry'
    elif grade_lepsze_rozwiazanie >= 4:
        return 'dobry'
    elif grade_lepsze_rozwiazanie >= 3:
        return 'dostateczny'
    else:
        return 'niedostateczny'

if __name__ == '__main__':
    # ĆWICZENIE 1
    display_speed_information(50)
    display_speed_information(49)
    display_speed_information(51)

    # ĆWICZENIE 2
    check_temperature_and_pressure(0, 1013)
    check_temperature_and_pressure(1, 1013)
    check_temperature_and_pressure(1, 1014)
    check_temperature_and_pressure(0, 1014)

    # ĆWICZENIE 3
    print_the_value_of_speeding_fine_in_built_up_area(49)
    print_the_value_of_speeding_fine_in_built_up_area(50)
    print_the_value_of_speeding_fine_in_built_up_area(51)
    print_the_value_of_speeding_fine_in_built_up_area(99)
    print_the_value_of_speeding_fine_in_built_up_area(100)
    print_the_value_of_speeding_fine_in_built_up_area(101)

    # ĆWICZENIE 4
    print(return_grade_info(1.9))
    print(return_grade_info(2))
    print(return_grade_info(2.1))
    print(return_grade_info(2.9))
    print(return_grade_info(3))
    print(return_grade_info(3.1))
    print(return_grade_info(3.9))
    print(return_grade_info(4))
    print(return_grade_info(4.1))
    print(return_grade_info(4.4))
    print(return_grade_info(4.5))
    print(return_grade_info(4.6))
    print(return_grade_info(4.9))
    print(return_grade_info(5))
    print(return_grade_info(5.1))

    # ĆWICZENIE 4 - lepsze rozwiązanie
    print(return_grade_info_lepsze_rozwiazanie(1.9))
    print(return_grade_info_lepsze_rozwiazanie(2))
    print(return_grade_info_lepsze_rozwiazanie(2.1))
    print(return_grade_info_lepsze_rozwiazanie(2.9))
    print(return_grade_info_lepsze_rozwiazanie(3))
    print(return_grade_info_lepsze_rozwiazanie(4))
    print(return_grade_info_lepsze_rozwiazanie(4.5))
    print(return_grade_info_lepsze_rozwiazanie(4.7))
    print(return_grade_info_lepsze_rozwiazanie(5))
    print(return_grade_info_lepsze_rozwiazanie(6))

