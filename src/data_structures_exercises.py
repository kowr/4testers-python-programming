def calculate_sum_of_numbers_from_the_list(january):
    return sum(january)

def calculate_average_of_list_numbers(input_list):
    return sum(input_list) / len(input_list)

def calculate_average_of_two_numbers(a, b):
    return (a + b) / 2


if __name__ == '__main__':
    january = [-4, 1.8, -7, 2]
    february = [-13, -9, -3, 3]
    january_average = calculate_average_of_list_numbers(january)
    february_average = calculate_average_of_list_numbers(february)
    print(calculate_sum_of_numbers_from_the_list(january))
    print(calculate_average_of_list_numbers(january_average))

