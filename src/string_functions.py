def print_welcome_message(person_name, city):
    print(f"Witaj {person_name}! Miło Cię widzieć w naszym mieście: {city}!")

def generate_email_address(person_first_name, person_last_name):
    print(f"{person_first_name.lower()}.{person_last_name.lower()}@4testers.pl")

if __name__ == '__main__':
    print_welcome_message("Krzysiek", "Kraków")
    print_welcome_message("Ania", "Łódź")

    generate_email_address("Janusz", "Nowak")
    generate_email_address("Barbara", "Kowalska")
