# import modułu - biblioteki random
from random import randint
from math import pi
from math import sqrt


def print_hello_40_times():
    for i in range(1, 41):
        print('Hello!', i)


def print_positive_numbers_from_1_to_30():
    for i in range(1, 31):
        print(i)


def print_positive_numbers(start, stop):
    for i in range(start, stop + 1):
        print(i)


def print_positive_numbers_divisible_by_7(startt, stopp):
    for i in range(startt, stopp):
        if i % 7 == 0:
            print(i)


def print_n_random_numbers(n):
    # dla i w range n mogę nie pisać od 0 do n tylko daję samo n - jeśli nie interesuje mnie wartość iteracji
    for i in range(n):
        print(randint(999, 10000))


def print_square_roots_of_numbers(list_of_numbers):
    for number in list_of_numbers:
        print(sqrt(number))


def get_square_roots_of_numbers(list_of_numberss):
    square_roots = []
    for numberr in list_of_numberss:
        sqrt_of_numberr = round(sqrt(numberr, 2))
        square_roots.append(sqrt(numberr))
        return square_roots

if __name__ == '__main__':
    print_hello_40_times()
    print_positive_numbers_from_1_to_30()
    print_positive_numbers(1, 30)
    print_positive_numbers_divisible_by_7(1, 101)
    print_n_random_numbers(5)
    print(sqrt(5))
    list_of_measurement_results = [11.0, 11, 69, 77, 32, 99.99, 25]
    print_square_roots_of_numbers(list_of_measurement_results)
    get_square_roots_of_numbers(list_of_measurement_results)
    list_of_measurement_resultss = [11.0, 11, 69, 77, 32, 99.99, 25]
    square_roots_of_measurements = get_square_roots_of_numbers(list_of_measurement_resultss)
    print(get_square_roots_of_numbers())
