movies = ['Interstellar', 'THX 1138', '21', 'Home Alone', 'Pulp Fiction']

last_movie = movies[-1]
movies.append('Scarface')
movies.append('Forrest Gump')
print(len(movies))
print(movies[-1])
middle_movies = movies[2:5]

print(middle_movies)

movies.insert(1, 'Joker')
print(movies)
print(middle_movies)


emails = ['a@example.com', 'b@example.com']

print(len(emails))
print(emails[0])
print(emails[-1])
emails.append('cde@example.com')
print(emails)


friend = {
    "first_name": "Adam",
    "age": 43,
    "hobby": ["games", "biochemistry"]
}

friend_hobbies = friend['hobby']
print("Hobbies of my friend are:", friend_hobbies)
print(f"My friend has {len(friend_hobbies)} hobbies.")

friend["hobby"].append("tenis")
print(friend)

friend["married"] = "True"
print(friend)
