def calculate_squared_number(number):
    return number ** 2


def convert_temperature_celcius_to_fahrenheit(temperature_in_celcius):
    return temperature_in_celcius * 9 / 5 + 32


def calculate_volume_of_cuboid(a, b, c):
    return a * b * c


if __name__ == '__main__':
    # zadanie 1
    # square_1 = calculate_square_number(0)
    # square_2 = calculate_square_number(16)
    # square_3 = calculate_square_number(2.55)
    # print(square_1)
    # print(square_2)
    # print(square_3)

    print(calculate_squared_number(0))
    print(calculate_squared_number(16))
    print(calculate_squared_number(2.55))

    # Zadanie 2 - konwersja stopni Celsjusza na stopnie Fahrenheita
    print(convert_temperature_celcius_to_fahrenheit(20))

    # Zadanie 3 - obliczanie objętości
    print(calculate_volume_of_cuboid(3, 5, 7))
