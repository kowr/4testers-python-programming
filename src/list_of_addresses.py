addresses = [
    {'city': 'Łódź', 'street': 'Piotrkowska', 'house_number': '5', 'post_code': '90-159'},
    {'city': 'Warszawa', 'street': 'Marszałkowska', 'house_number': '7', 'post_code': '00-357'},
    {'city': 'Lublin', 'street': 'Krakowskie Przedmieście', 'house_number': '13', 'post_code': '20-753'},
]

if __name__ == '__main__':
    print(addresses[2]['post_code'])
    print(addresses[1]['city'])
    addresses[0]['street'] = 'Włókiennicza'

    print(addresses)

    print(addresses[0])
    print(addresses[1])
    print(addresses[2])
